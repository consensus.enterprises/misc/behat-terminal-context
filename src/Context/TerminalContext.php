<?php

namespace Consensus\BehatTerminalContext\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Defines application features from the specific context.
 */
class TerminalContext implements SnippetAcceptingContext {

  protected $debug = FALSE;

  protected $ignoreFailures = FALSE;

  protected $process;

  protected $tempDir;

  protected $orig_dir;

  // An array of file hashes keyed by filename. Used to know if files have changed.
  protected $file_hashes = [];

  // An array of file timestamps keyed by filename. Used to know if files have been updated.
  protected $file_timestamps = [];

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
    $this->setOrigDir();
  }

  /**
   * Clean up temporary directories.
   */
  public function __destruct() {
    $this->rmdir($this->tempDir);
  }

  protected function getOrigDir() {
    return $this->orig_dir;
  }

  protected function setOrigDir() {
    if (!isset($this->orig_dir)) {
      $this->orig_dir = getcwd();
    }
  }

  protected function getOutput() {
    if ($this->process->isSuccessful()) {
      return $this->process->getOutput();
    }
    $result[] = 'Output:';
    $result[] = '================';
    $result[] = $this->process->getOutput();

    $result[] = 'Error Output:';
    $result[] = "================\n";
    $result[] = $this->process->getErrorOutput();
    return implode("\n", $result);
  }

  protected function getStdErr() {
    return $this->process->getErrorOutput();
  }

  /**
   * Run a command in a sub-process, and set its output.
   */
  protected function exec($command) {
    $this->process = new Process(["bash", "-c", $command]);
    $this->process->setTimeout(300);
    $this->process->run();

    $output = $this->getOutput();
    $this->printDebug($output);
  }

  /**
   * Run a series of steps on the command line.
   */
  protected function runSteps($steps) {
    foreach ($steps as $step) {
      $this->succeed($step);
    }
  }

  protected function printDebug(string $output) {
    if (!$this->debug) return;
    if (empty($output)) return;
    print_r("--- DEBUG START ---\n");
    print_r($output);
    print_r("\n--- DEBUG END -----\n");
  }

  protected function succeed($command) {
    $this->exec($command);

    if (!$this->process->isSuccessful()) {
      throw new ProcessFailedException($this->process);
    }
  }

  /**
   * Run a command that is expected to fail in a sub-process, and set its output.
   */
  protected function fail($command) {
    $this->exec($command);

    if ($this->process->isSuccessful()) {
      throw new \RuntimeException("Command that was expected to fail succeeded.");
    }
  }

  /**
   * Create a temporary directory
   */
  protected function makeTempDir() {
    $tempfile = tempnam(sys_get_temp_dir(), 'behat_cli_');
    if (file_exists($tempfile)) {
      unlink($tempfile);
    }
    mkdir($tempfile);
    $this->tempDir = $tempfile;
  }

  /**
   * Get a temporary directory path.
   */
  protected function getTempDir() {
    if (!isset($this->tempDir)) {
      $this->makeTempDir();
    }
    return $this->tempDir;
  }

  /**
   * Recursively delete a directory and its contents.
   */
  protected function rmdir($dir) {
    $this->iRun('rm -rf ' . $dir);
  }

  /**
   * Set a debug flag when running scenarios tagged @debug.
   *
   * @BeforeScenario @debug
   */
  public function setDebugFlag() {
    $this->debug = TRUE;
  }

  /**
   * In case we switched to a temporary directory, switch back to the original
   * directory before the next scenario.
   *
   * @AfterScenario
   */
  public function returnToOrigDir() {
    chdir($this->getOrigDir());
  }

  /**
   * @When I run :command
   */
  public function iRun($command)
  {
    if ($this->ignoreFailures) {
      return $this->exec($command);
    }
    $this->succeed($command);
  }

  /**
   * @When I try to run :command
   */
  public function iTry($command)
  {
    return $this->exec($command);
  }

  /**
   * @When I fail to run :command
   */
  public function iFail($command)
  {
    return $this->fail($command);
  }

  /**
   * @Then I should get:
   */
  public function iShouldGet(PyStringNode $expectedOutput)
  {
    $output = $this->getOutput() . $this->getStdErr();
    foreach ($expectedOutput->getStrings() as $string) {
      $string = trim($string);
      if (!empty($string) && strpos($output, $string) === FALSE) {
        throw new \Exception("'$string' was not found in command output:\n------\n" . $output . "\n------\n");
      }
    }
  }

  /**
   * @And I get:
   */
  public function iGet(PyStringNode $unexpectedOutput)
  {
    return $this->iShouldGet($unexpectedOutput);
  }

  /**
   * @Then I should not get:
   */
  public function iShouldNotGet(PyStringNode $unexpectedOutput)
  {
    $output = $this->getOutput() . $this->getStdErr();
    foreach ($unexpectedOutput->getStrings() as $string) {
      $string = trim($string);
      if (!empty($string) && strpos($output, $string) !== FALSE) {
        throw new \RuntimeException("'$string' was found in command output:\n------\n" . $this->getOutput() . "\n------\n");
      }
    }
  }

  /**
   * @And I do not get:
   */
  public function iDoNotGet(PyStringNode $unexpectedOutput)
  {
    return $this->iShouldNotGet($unexpectedOutput);
  }

  /**
   * @Given I am in a temporary directory
   */
  public function iAmInATemporaryDirectory()
  {
    $this->makeTempDir();
    chdir($this->tempDir);
  }

  /**
   * @Given I am in the :dir directory
   */
  public function iAmInTheDirectory($dir)
  {
    chdir($dir);
  }

  /**
   * Execute a script in our project, even if we've moved to a temporary directory.
   *
   * @When I execute :script
   */
  public function iExecute($script)
  {
    $script = $this->getOrigDir() . DIRECTORY_SEPARATOR . $script;
    $this->succeed($script);
  }

  /**
   * @Then executing :script should fail
   */
  public function executingShouldFail($script)
  {
    $script = $this->getOrigDir() . DIRECTORY_SEPARATOR . $script;
    $this->fail($script);
  }

  /**
   * @Given executing :script fails
   */
  public function executingFails($script)
  {
    $this->executingShouldFail($script);
  }

  /**
   * @Then the following files should exist:
   */
  public function theFollowingFilesShouldExist(PyStringNode $files)
  {
     foreach ($files->getStrings() as $file) {
      if (!file_exists($file)) {
        throw new \RuntimeException("Expected file '$file' was not found.");
      }
    }
  }

  /**
   * @Then the file :file exists
   */
  public function theFileExists($file)
  {
    if (!file_exists($file)) {
      throw new \RuntimeException("Expected file '$file' was not found.");
    }
  }

  /**
   * @Then the file :file does not exist
   */
  public function theFileDoesNotExist($file)
  {
    if (file_exists($file)) {
      throw new \RuntimeException("Unexpected file '$file' was found.");
    }
  }

  /**
   * @Then the :directory directory should exist
   */
  public function theDirectoryShouldExist($directory)
  {
    if (!is_dir($directory)) {
      throw new \RuntimeException("Expected directory '$directory' was not found.");
    }
  }

  /**
   * @Then the :directory directory should not exist
   */
  public function theDirectoryShouldNotExist($directory)
  {
    if (is_dir($directory)) {
      throw new \RuntimeException("Unexpected directory '$directory' was found.");
    }
  }

  /**
   * @Given the following files exist:
   */
  public function theFollowingFilesExist(PyStringNode $files)
  {
    $this->theFollowingFilesShouldExist($files);
  }

  /**
   * @Then the following files should not exist:
   */
  public function theFollowingFilesShouldNotExist(PyStringNode $files)
  {
    foreach ($files->getStrings() as $file) {
      if (file_exists($file)) {
        throw new \RuntimeException("Unexpected file '$file' was found.");
      }
    }
  }

  /**
   * @Given the following files do not exist:
   */
  public function theFollowingFilesDoNotExist(PyStringNode $files)
  {
    $this->theFollowingFilesShouldNotExist($files);
  }

  /**
   * @Then the file :file should contain:
   */
  public function theFileShouldContain($file, PyStringNode $lines)
  {
    clearstatcache(true);
    $file = realpath($file);
    $contents = file_get_contents($file);
    foreach ($lines->getStrings() as $line) {
      if (strlen($line) == 0) continue; # Skip empty lines.
      if (strpos($contents, $line) === FALSE) {
        throw new \RuntimeException("'$line' was not found in '$file'.");
      }
    }
  }

  /**
   * @Given the file :file contains:
   */
  public function theFileContains($file, PyStringNode $lines)
  {
    $this->theFileShouldContain($file, $lines);
  }

  /**
   * @Then the file :file should not contain:
   */
  public function theFileShouldNotContain($file, PyStringNode $lines)
  {
    clearstatcache(true);
    $file = realpath($file);
    $contents = file_get_contents($file);
    foreach ($lines->getStrings() as $line) {
      if (strlen($line) == 0) continue; # Skip empty lines.
      if (strpos($contents, $line) !== FALSE) {
        throw new \RuntimeException("'$line' was unexpectedly found in '$file'.");
      }
    }
  }

  /**
   * @Given the file :file does not contain:
   */
  public function theFileDoesNotContain($file, PyStringNode $lines)
  {
    $this->theFileShouldNotContain($file, $lines);
  }

  /**
   * Return the hash of a given file's contents.
   */
  protected function getFileHash($filename) {
    $this->succeed("sudo cat ${filename}");
    return hash('md5', $this->getOutput());
  }

  /**
   * @Given I record a reference hash of :filename
   */
  public function recordReferenceHash($filename)
  {
    $this->file_hashes[$filename] = $this->getFileHash($filename);
  }

  /**
   * @Then file :filename has not changed
   */
  public function fileHasNotChanged($filename)
  {
    if ($this->file_hashes[$filename] !== $this->getFileHash($filename)) {
      throw new \Exception("The file ${filename} was not expected to have changed, but has changed.");
    }
  }

  /**
   * @Then file :filename has changed
   */
  public function fileHasChanged($filename)
  {
    if ($this->file_hashes[$filename] === $this->getFileHash($filename)) {
      throw new \Exception("The file ${filename} was expected to have changed, but has not changed.");
    }
  }

  /**
   * Return the timestamp of a given file.
   */
  protected function getFileTimestamp($filename) {
    $this->succeed("sudo date -r ${filename} \"+%s\"");
    return $this->getOutput();
  }

  /**
   * @Given I record the timestamp of :filename
   */
  public function recordTimestamp($filename)
  {
    $this->file_timestamps[$filename] = $this->getFileTimestamp($filename);
  }

  /**
   * @Then file :filename has not been updated
   */
  public function fileHasNotBeenUpdated($filename)
  {
    if ($this->file_timestamps[$filename] !== $this->getFileTimestamp($filename)) {
      throw new \Exception("The file ${filename} was not expected to be updated, but has been updated.");
    }
  }

  /**
   * @Then file :filename has been updated
   */
  public function fileHasBeenUpdated($filename)
  {
    if ($this->file_timestamps[$filename] === $this->getFileTimestamp($filename)) {
      throw new \Exception("The file ${filename} was expected to be updated, but has not been updated.");
    }
  }

}
