# behat-terminal-context

```yaml
composer require consensus/behat-terminal-context
```

## TerminalContext
Support for performing tests in a linux terminal.
### Enabling
**behat.yml**

```yaml
default:
  suites:
    default:
      contexts:
        - Consensus\BehatTerminalContext\Context\TerminalContext
```
